{-# LANGUAGE NumericUnderscores  #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main
    ( main
    ) where

import           Control.Concurrent
import           Control.Exception
import           Control.Monad                           (forM_, when)
import           Control.Monad.IO.Class                  (MonadIO (..))
import           Data.Aeson                              (Result (..), ToJSON, decode, encode, fromJSON)
import qualified Data.ByteString.Lazy.Char8              as B8
import qualified Data.ByteString.Lazy                    as LB
import           Data.Monoid                             (Last (..))
import           Data.Proxy                              (Proxy (..))
import           Data.String                             (IsString (..))
import           Data.Text                               (Text, pack)
import           Data.UUID                               hiding (fromString)
import           Ledger.Value                           -- TODO uncomment (AssetClass (..), CurrencySymbol, Value, flattenValue, TokenName)
import           Network.HTTP.Req
import qualified Mirqur                as MQ
import           Plutus.PAB.Events.ContractInstanceState (PartiallyDecodedResponse (..))
import           Plutus.PAB.Webserver.Types
import           System.Environment                      (getArgs)
import           System.Exit                             (exitFailure)
import           Text.Printf                             (printf)
import           Text.Read                               (readMaybe)
import           Wallet.Emulator.Types                   (Wallet (..))

import           MirqurDemo                                 (cidFile, MirqurContracts)

main :: IO ()
main = do
    w   <- Wallet . read . head <$> getArgs
    cid <- read                 <$> readFile (cidFile w)
    mcs <- decode               <$> LB.readFile "symbol.json"
    case mcs of
        Nothing -> putStrLn "invalid symbol.json" >> exitFailure
        Just cs -> do
            putStrLn $ "cid: " ++ show cid
            putStrLn $ "symbol: " ++ show (cs :: CurrencySymbol)
            go cid cs
  where
    go :: UUID -> CurrencySymbol -> IO a
    go cid cs = do
        cmd <- readCommandIO
        case cmd of
            Funds                    -> getFunds cid
            Pools                    -> getPools cid
            Create lpid        -> do
                putStrLn "Enter balances and weights (optional, default: 1) for each token, separated by space:"
                log <- readOpenIO lpid []
                let params = toCreateParams cs log
                putStrLn "Create-Params:"
                putStrLn $ show params
                putStrLn "cid:"
                putStrLn $ show cid
                openPool cid params
            Add lpid               -> do
                putStrLn "Enter deposits for each token:"
                log <- readDeltaValueIO lpid []
                let params = toAddParams cs lpid log
                putStrLn "Add-Params:"
                putStrLn $ show params
                putStrLn "cid:"
                putStrLn $ show cid
                addLiquidity cid params
            Remove lpid            ->  do
                putStrLn "Enter withdrawals for each token:"
                log <- readDeltaValueIO lpid []
                let params = toRemoveParams cs lpid log
                putStrLn "Remove-Params:"
                putStrLn $ show params
                putStrLn "cid:"
                putStrLn $ show cid
                removeLiquidity cid params
            Close lpid                 ->  do
                let params = toCloseParams cs lpid
                putStrLn "Close-Params:"
                putStrLn $ show params
                putStrLn "cid:"
                putStrLn $ show cid
                closePool cid params -- closePool cid $ toCloseParams cs lp
            Swap lpid amt inA outA     -> do
                let params = toSwapParams cs lpid amt inA outA
                putStrLn "Swap-Params:"
                putStrLn $ show params
                putStrLn "cid:"
                putStrLn $ show cid
                swap cid params -- swap cid $ toSwapParams cs lp amt inA outA

            -- Open balWeights        -> openPool cid $ toOpenParams cs balWeights
            -- Add lp bal               -> addLiquidity cid $ toAddParams cs lp bal
            -- Remove lp bal            -> removeLiquidity cid $ toRemoveParams cs lp bal
            -- Close lp                 -> closePool cid $ toCloseParams cs lp
            -- Swap lp amt inA outA       -> swap cid $ toSwapParams cs lp amt inA outA
        go cid cs

type LPID = [Char]
type V = [(Char, Integer)]

data Command =
      Funds
    | Pools -- token  weight  balance
    | Open String--LPID--[(Char, Integer, Integer)] -- Integer Char Integer Char
    | Add LPID--LPID V -- Integer Char Integer Char
    | Remove LPID--LPID V -- Integer Char Char
    | Close LPID -- Char Char
    | Swap LPID Integer Char Char -- Integer Char Char
    deriving (Show, Read, Eq, Ord)

readCommandIO :: IO Command
readCommandIO = do
    putStrLn "\nEnter a command:\n Funds, Pools, Create \"pool-tokens\", Add \"pool-tokens\", Remove \"pool-tokens\", Close \"pool-tokens\", Swap \"pool-tokens\" amt 'inTkn' 'outTkn'\n\nexample:\n Swap \"ABC\" 500 'B' 'A'\nto swap 500 B's for A's with pool ABC\n(note the quotes!)\n"
    s <- getLine
    maybe readCommandIO return $ readMaybe s


readOpenIO :: LPID -> [(Char, Integer, Integer)]  -> IO [(Char, Integer, Integer)] 
readOpenIO [] log = do
    putStrLn "creating pool:"
    print log
    return log
readOpenIO (l:pid) log = do 
    putStrLn (l:": ")
    s <- getLine 
    let ints = map read (words s) :: [Integer]
    case ints of 
        [] -> do
            putStrLn "balance cannot be empty"
            readOpenIO (l:pid) log
        [b] 
            | b <= 0 -> do 
                putStrLn "initial balance needs to be positive"
                readOpenIO (l:pid) log
            | otherwise -> readOpenIO pid $ log ++ [(l, b, 1)]
        [b ,w] 
            | b <= 0 -> do 
                putStrLn "initial balance needs to be nonzero positive"
                readOpenIO (l:pid) log
            | w <= 0 -> do 
                putStrLn "initial weight needs to be nonzero positive"
                readOpenIO (l:pid) log
            | otherwise -> readOpenIO pid $ log ++ [(l, b, w)]

readDeltaValueIO :: LPID -> V -> IO V
readDeltaValueIO [] log = do
    putStrLn "pool delta:"
    print log
    return log
readDeltaValueIO (l:pid) log = do 
    putStrLn (l:": ")
    s <- getLine 
    let int = readMaybe s :: Maybe Integer
    case int of 
        Nothing -> do 
            putStrLn "defaulting to 0"
            readDeltaValueIO pid $ log ++ [(l, 0)]
        Just i 
            | i < 0 -> do 
                putStrLn "change cannot be negative"
                readDeltaValueIO (l:pid) log
            |otherwise -> readDeltaValueIO pid $ log ++ [(l, i)]


toAsset :: CurrencySymbol -> Char -> AssetClass
toAsset cs tn = assetClass cs (fromString [tn])

-- TODO check if we need sort here
toOffChainLPID :: CurrencySymbol -> LPID -> [AssetClass]
toOffChainLPID cs = map (\t -> assetClass cs (fromString [t])) 

toOffChainV :: CurrencySymbol -> V -> [(AssetClass, Integer)]
toOffChainV cs = map (\(t, a) -> (assetClass cs (fromString [t]), a)) 

toLP :: CurrencySymbol -> [Char] -> MQ.LiquidityPool
toLP cs = MQ.LiquidityPool . MQ.Weights . foldr1 (<>) . map (\tkn -> singleton cs (fromString [tkn]) 1)

toBal :: CurrencySymbol -> V -> Value
toBal cs = foldr1 (<>) . map (\(tkn, bal) -> singleton cs (fromString [tkn]) bal)


toOpenParams :: CurrencySymbol -> [(Char, Integer, Integer)] -> MQ.OpenParams
toOpenParams cs bw = MQ.OpenParams
                        { MQ.openWeights = toOffChainV cs w
                        , MQ.openBalances = toOffChainV cs b
                        }
  where
    w = map (\(x, _, y) -> (x, y)) bw
    b = map (\(x, y, _) -> (x, y)) bw
toAddParams :: CurrencySymbol -> LPID -> V -> MQ.AddParams
toAddParams cs lp bal = MQ.AddParams
    { MQ.addPool = toOffChainLPID cs lp-- LiquidityPool -- the pool to add to. Note that this has contain the balances-coins but may have more
    , MQ.addBalances = toOffChainV cs bal--Value -- the coin-balances to add
    } 
toRemoveParams :: CurrencySymbol -> LPID -> V -> MQ.RemoveParams
toRemoveParams cs lp bal = MQ.RemoveParams
    { MQ.removePool = toOffChainLPID cs lp -- LiquidityPool -- weights are irrelevant here because == only checks that coins align
    , MQ.removeLiquidity = toOffChainV cs bal--Value -- ^ The amount of liquidity tokens to burn in exchange for liquidity from the pool. Denominated in original currency, not liquidity coins 
    }


toCloseParams :: CurrencySymbol -> LPID -> MQ.CloseParams
toCloseParams cs lp = MQ.CloseParams{MQ.closedPool = toOffChainLPID cs lp}

toSwapParams :: CurrencySymbol -> LPID -> Integer -> Char -> Char -> MQ.SwapParams
toSwapParams cs lp amt inA outA = MQ.SwapParams
                                    { MQ.swapPool = toOffChainLPID cs lp -- LiquidityPool
                                    , MQ.inAmt = amt     
                                    , MQ.inAsset = assetClass cs $ fromString [inA]        -- ^ One 'Coin' of the liquidity pair.
                                    , MQ.outAsset = assetClass cs $ fromString [outA]           -- ^ The other 'Coin'.  -- ^ The amount the first 'Coin' that should be swapped.
                                    }
showCoinHeader :: IO ()
showCoinHeader = printf "\n                                                 currency symbol                                                         token name          amount\n\n"

showCoin :: CurrencySymbol -> TokenName -> Integer -> IO ()
showCoin cs tn = printf "%64s %66s %15d\n" (show cs) (show tn)

getFunds :: UUID -> IO ()
getFunds cid = do
    callEndpoint cid "funds" ()
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right (MQ.Funds v) -> showFunds v
            _                  -> go

    showFunds :: Value -> IO ()
    showFunds v = do
        showCoinHeader
        forM_ (flattenValue v) $ \(cs, tn, amt) -> showCoin cs tn amt
        printf "\n"

getPools :: UUID -> IO ()
getPools cid = do
    callEndpoint cid "pools" ()
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right (MQ.Pools ps) -> showPools ps
            _                   -> go

    showPools :: [(MQ.LiquidityPool, MQ.UnpackedLiquidity)] -> IO ()
    showPools ps = do
        forM_ ps $ \(MQ.LiquidityPool (MQ.Weights weights), (MQ.UnpackedLiquidity v)) -> do
            printf "\nbalances:"
            showCoinHeader
            forM_ (flattenValue v) $ \(cs, tn, amt) -> showCoin cs tn amt
            printf "\nweights:"
            showCoinHeader
            -- TODO revert dummy
            forM_ (flattenValue weights) $ \(cs, tn, w) -> showCoin cs tn w
            -- forM_ (rFlattenValue weights) $ \(cs, tn, w) -> rShowCoin cs tn w
            printf "\n\n"
    -- showPools :: [((US.Coin US.A, US.Amount US.A), (US.Coin US.B, US.Amount US.B))] -> IO ()
    -- showPools ps = do
    --     forM_ ps $ \((US.Coin (AssetClass (csA, tnA)), amtA), (US.Coin (AssetClass (csB, tnB)), amtB)) -> do
    --         showCoinHeader
    --         showCoin csA tnA (US.unAmount amtA)
    --         showCoin csB tnB (US.unAmount amtB)

openPool :: UUID -> MQ.OpenParams -> IO ()
openPool cid cp = do
    callEndpoint cid "open" cp
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right MQ.Opened -> putStrLn "opened"
            Left err'        -> putStrLn $ "error: " ++ show err'
            _                -> go

addLiquidity :: UUID -> MQ.AddParams -> IO ()
addLiquidity cid ap = do
    callEndpoint cid "add" ap
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right MQ.Added -> putStrLn "added"
            Left err'      -> putStrLn $ "error: " ++ show err'
            _              -> go

removeLiquidity :: UUID -> MQ.RemoveParams -> IO ()
removeLiquidity cid rp = do
    callEndpoint cid "remove" rp
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right MQ.Removed -> putStrLn "removed"
            Left err'        -> putStrLn $ "error: " ++ show err'
            _                -> go

closePool :: UUID -> MQ.CloseParams -> IO ()
closePool cid cp = do
    callEndpoint cid "close" cp
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right MQ.Closed -> putStrLn "closed"
            Left err'       -> putStrLn $ "error: " ++ show err'
            _               -> go

swap :: UUID -> MQ.SwapParams -> IO ()
swap cid sp = do
    callEndpoint cid "swap" sp
    threadDelay 2_000_000
    go
  where
    go = do
        e <- getStatus cid
        case e of
            Right MQ.Swapped -> putStrLn "swapped"
            Left err'        -> putStrLn $ "error: " ++ show err'
            _                -> go

getStatus :: UUID -> IO (Either Text MQ.UserContractState)
getStatus cid = runReq defaultHttpConfig $ do
    liftIO $ printf "\nget request to 127.0.1:9080/api/contract/instance/%s/status\n" (show cid)
    w <- req
        GET
        (http "127.0.0.1" /: "api"  /: "contract" /: "instance" /: pack (show cid) /: "status")
        NoReqBody
        (Proxy :: Proxy (JsonResponse (ContractInstanceClientState MirqurContracts)))
        (port 9080)
    case fromJSON $ observableState $ cicCurrentState $ responseBody w of
        Success (Last Nothing)  -> liftIO $ threadDelay 1_000_000 >> getStatus cid
        Success (Last (Just e)) -> return e
        _                       -> liftIO $ ioError $ userError "error decoding state"

callEndpoint :: ToJSON a => UUID -> String -> a -> IO ()
callEndpoint cid name a = handle h $ runReq defaultHttpConfig $ do
    liftIO $ printf "\npost request to 127.0.0.1:9080/api/contract/instance/%s/endpoint/%s\n" (show cid) name
    liftIO $ printf "request body: %s\n\n" $ B8.unpack $ encode a
    v <- req
        POST
        (http "127.0.0.1" /: "api"  /: "contract" /: "instance" /: pack (show cid) /: "endpoint" /: pack name)
        (ReqBodyJson a)
        (Proxy :: Proxy (JsonResponse ()))
        (port 9080)
    when (responseStatusCode v /= 200) $
        liftIO $ ioError $ userError $ "error calling endpoint " ++ name
  where
    h :: HttpException -> IO ()
    h = ioError . userError . show
