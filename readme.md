# what is this

Some outdated version of MetaDEX (formerly Mirqur) Plutus code with the best parts chopped out - you might however still enjoy the scaling solution.


# how to run (outdated by ~7 months)

1. install nix and plutus first, following those instructions: https://github.com/input-output-hk/plutus. Make sure to add the IOHK caches or wait a few hours for cabal to compile from scratch

2. start pab
```
cd plutus 
git checkout plutus-starter-devcontainer/v1.0.8
nix-shell
cd ..
git clone git@gitlab.com:fthagn/mirqur_plutus_2.git
cd mirqur_plutus_2/demo
cabal run mirqur-pab 
```

3. in up to four new terminals, do this, replacing the 1 with 2/3/4, respectively
```
cd plutus
nix-shell 
cd ../mirqur_plutus_2/demo 
cabal run mirqur-client -- 1
```
