{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE DerivingVia           #-}
{-# options_ghc -Wno-redundant-constraints #-}
{-# options_ghc -fno-strictness            #-}
{-# options_ghc -fno-specialise            #-}

module Types
  where

import           Ledger
import           Ledger.Value        hiding (valueOf) -- TODO uncomment (AssetClass (..), assetClass, assetClassValue, assetClassValueOf)
import           Playground.Contract (FromJSON, Generic, ToJSON, ToSchema)
import qualified PlutusTx
import           PlutusTx.Prelude
import qualified Prelude             as Haskell
import           Text.Printf         (PrintfArg)

-- new imports
import           PlutusTx.These
import qualified PlutusTx.AssocMap                as Map
import qualified Plutus.V1.Ledger.Value     as V
import GHC.Unicode (GeneralCategory(CurrencySymbol)) -- TODO still needed?

-- import           Codec.Serialise.Class            (Serialise)
-- import           Data.Hashable                    (Hashable)
-- import           Control.DeepSeq                  (NFData)
-- import           Data.Text.Prettyprint.Doc
-- import           Data.Text.Prettyprint.Doc.Extras

-- TODO set this as pool-parameter and/or DEX-parameter
swapFee = 3 % 1000
-- swapFeeRemainder = (fromInteger 1) - swapFee

-- | Mirqur coin token
data M = M deriving (Haskell.Show, Haskell.Eq, Generic)
PlutusTx.makeIsDataIndexed ''M [('M, 0)]
PlutusTx.makeLift ''M

-- | "A"-side coin token
data A = A
PlutusTx.makeIsDataIndexed ''A [('A, 0)]
PlutusTx.makeLift ''A

-- | "B"-side coin token
data B = B
PlutusTx.makeIsDataIndexed ''B [('B, 0)]
PlutusTx.makeLift ''B

-- | Pool-state coin token
data PoolState = PoolState
PlutusTx.makeIsDataIndexed ''PoolState [('PoolState, 0)]
PlutusTx.makeLift ''PoolState

-- | Liquidity-state coin token
data Liquidity = Liquidity
PlutusTx.makeIsDataIndexed ''Liquidity [('Liquidity, 0)]
PlutusTx.makeLift ''Liquidity

-- | A single 'AssetClass'. Because we use three coins, we use a phantom type to track
-- which one is which.
newtype Coin a = Coin { unCoin :: AssetClass }
  deriving stock   (Haskell.Show, Generic)
  deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq, Haskell.Ord)
PlutusTx.makeIsDataIndexed ''Coin [('Coin, 0)]
PlutusTx.makeLift ''Coin

-- | Likewise for 'Integer'; the corresponding amount we have of the
-- particular 'Coin'.
newtype Amount a = Amount { unAmount :: Integer }
  deriving stock   (Haskell.Show, Generic)
  deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Ord, PrintfArg)
  deriving newtype (Haskell.Eq, Haskell.Ord, Haskell.Num)
  deriving newtype (AdditiveGroup, AdditiveMonoid, AdditiveSemigroup, MultiplicativeSemigroup)
PlutusTx.makeIsDataIndexed ''Amount [('Amount, 0)]
PlutusTx.makeLift ''Amount

{-# INLINABLE valueOf #-}
valueOf :: Coin a -> Amount a -> Value
valueOf c a = assetClassValue (unCoin c) (unAmount a)

{-# INLINABLE unitValue #-}
unitValue :: Coin a -> Value
unitValue c = valueOf c 1

{-# INLINABLE isUnity #-}
isUnity :: Value -> Coin a -> Bool
isUnity v c = amountOf v c == 1

{-# INLINABLE isZero #-}
isZero :: Value -> Coin a -> Bool
isZero v c = amountOf v c == 0

{-# INLINABLE amountOf #-}
amountOf :: Value -> Coin a -> Amount a
amountOf v = Amount . assetClassValueOf v . unCoin

{-# INLINABLE mkCoin #-}
mkCoin:: CurrencySymbol -> TokenName -> Coin a
mkCoin c = Coin . assetClass c

{-# INLINABLE singleValue #-}
singleValue :: Value -> (AssetClass, Integer) 
singleValue v' = let v = V.flattenValue v' in 
    case v of
        [(cs, tkn, amt)] -> (assetClass cs tkn, amt) 
        _             -> traceError "not exactly one nonzero asset"

{-# INLINABLE zeroVal #-}
zeroVal :: Value 
zeroVal = Value $ Map.empty -- TODO does this work?

type EValue = Either BuiltinString Value

-- issued liquidity coins
newtype PackedLiquidity = PackedLiquidity { unwrapPacked :: Value } 
    deriving stock   (Haskell.Show, Generic)
    deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
    -- deriving stock (Generic)
    -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
    -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
    -- deriving Pretty via (PrettyShow Value)
PlutusTx.makeIsDataIndexed ''PackedLiquidity [('PackedLiquidity, 0)]
PlutusTx.makeLift ''PackedLiquidity

-- TODO applicative or smth?
unwrapETPacked :: Either BuiltinString PackedLiquidity -> EValue 
unwrapETPacked (Left t)  = Left t 
unwrapETPacked (Right l) = Right $ unwrapPacked l

-- record of issued liquidity coins
newtype UnpackedLiquidity = UnpackedLiquidity { unwrapUnpacked :: Value } 
    deriving stock   (Haskell.Show, Generic)
    deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
    -- deriving stock (Generic)
    -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
    -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
    -- deriving Pretty via (PrettyShow Value)
PlutusTx.makeIsDataIndexed ''UnpackedLiquidity [('UnpackedLiquidity, 0)]
PlutusTx.makeLift ''UnpackedLiquidity

-- TODO consider using below at some point for added type safety
-- -- record of issued liquidity coins
-- newtype OldBalance = OldBalance { unwrapOldB :: Value } 
--     deriving stock   (Haskell.Show, Generic)
--     deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
--     -- deriving stock (Generic)
--     -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
--     -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
--     -- deriving Pretty via (PrettyShow Value)
-- PlutusTx.makeIsDataIndexed ''OldBalance [('OldBalance, 0)]
-- PlutusTx.makeLift ''OldBalance


-- -- record of issued liquidity coins
-- newtype NewBalance = NewBalance { unwrapNewB :: Value } 
--     deriving stock   (Haskell.Show, Generic)
--     deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
--     -- deriving stock (Generic)
--     -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
--     -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
--     -- deriving Pretty via (PrettyShow Value)
-- PlutusTx.makeIsDataIndexed ''NewBalance [('NewBalance, 0)]
-- PlutusTx.makeLift ''NewBalance


-- -- record of issued liquidity coins
-- newtype DiffBalance = DiffBalance { unwrapDiff :: Value } 
--     deriving stock   (Haskell.Show, Generic)
--     deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
--     -- deriving stock (Generic)
--     -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
--     -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
--     -- deriving Pretty via (PrettyShow Value)
-- PlutusTx.makeIsDataIndexed ''DiffBalance [('UnpackedLiqDiffBalanceuidity, 0)]
-- PlutusTx.makeLift ''DiffBalance

-- coin weights
newtype Weights = Weights { unwrapWeights :: Value }
    deriving stock   (Haskell.Show, Generic)
    deriving newtype (ToJSON, FromJSON, ToSchema, Eq, Haskell.Eq)
    -- deriving stock (Generic)
    -- deriving anyclass (ToJSON, FromJSON, Hashable, NFData)
    -- deriving newtype (Serialise, PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)
    -- deriving Pretty via (PrettyShow Value)
PlutusTx.makeIsDataIndexed ''Weights [('Weights, 0)]
PlutusTx.makeLift ''Weights

newtype Mirqur = Mirqur
    { mqCoin :: Coin M
    } deriving stock    (Haskell.Show, Generic)
      deriving anyclass (ToJSON, FromJSON, ToSchema)
      deriving newtype  (Haskell.Eq, Haskell.Ord)
PlutusTx.makeIsDataIndexed ''Mirqur [('Mirqur, 0)]
PlutusTx.makeLift ''Mirqur

data LiquidityPool = LiquidityPool
    { weights :: Weights
    }
    deriving (Haskell.Show, Generic, ToJSON, FromJSON, ToSchema)
PlutusTx.makeIsDataIndexed ''LiquidityPool [('LiquidityPool, 0)]
PlutusTx.makeLift ''LiquidityPool

--forcefully exported from Plutus.V1.Ledger.Value
{-# INLINABLE unionVal #-}
-- | Combine two 'Value' maps
unionVal :: Value -> Value -> Map.Map CurrencySymbol (Map.Map TokenName (These Integer Integer))
unionVal (V.Value l) (V.Value r) =
    let
        combined = Map.union l r
        unThese k = case k of
            This a    -> This <$> a
            That b    -> That <$> b
            These a b -> Map.union a b
    in unThese <$> combined

--forcefully exported from Plutus.V1.Ledger.Value
{-# INLINABLE checkPred #-}
checkPred :: (These Integer Integer -> Bool) -> Value -> Value -> Bool
checkPred f l r =
    let
      inner :: Map.Map TokenName (These Integer Integer) -> Bool
      inner = Map.all f
    in
      Map.all inner (unionVal l r)

--forcefully exported from Plutus.V1.Ledger.Value
{-# INLINABLE checkBinRel #-}
-- | Check whether a binary relation holds for value pairs of two 'Value' maps,
--   supplying 0 where a key is only present in one of them.
checkBinRel :: (Integer -> Integer -> Bool) -> V.Value -> Value -> Bool
checkBinRel f l r =
    let
        unThese k' = case k' of
            This a    -> f a 0
            That b    -> f 0 b
            These a b -> f a b
    in checkPred unThese l r

{-# INLINABLE sameCoins #-}
sameCoins :: Value -> Value -> Bool
sameCoins x y = 
        let
            bothPos a b = (a > 0) && (b > 0)
            bothZero a b = (a == 0) && (b == 0)
            bothSame a b = bothPos a b || bothZero a b
        in 
            checkBinRel bothSame x y

instance Eq LiquidityPool where
    {-# INLINABLE (==) #-}
     -- Make sure the underlying coins aren't equal.
     -- TODO replace dummy version 
    x == y = sameCoins (unwrapWeights (weights x)) (unwrapWeights (weights y))
    -- x == y = sameCoins (fromValueWith toOne (weights x)) (fromValueWith toOne (weights y))

{-# INLINABLE hasCoins #-}
-- checks if all coins in second Value are also in first
hasCoins :: Value -> Value -> Bool
hasCoins = checkBinRel hasCoin 
  where
    hasCoin :: Integer -> Integer -> Bool 
    hasCoin a b = (a > 0) || (b == 0)

{-# INLINABLE weightsHaveCoins #-}
weightsHaveCoins :: Weights -> Value -> Bool 
weightsHaveCoins Weights{..} c = unwrapWeights `hasCoins` c

{-# INLINABLE poolHasCoins #-}
poolHasCoins :: LiquidityPool -> Value -> Bool 
poolHasCoins LiquidityPool{..} c = weights `weightsHaveCoins` c

{-# INLINABLE isSingletonOf #-}
isSingletonOf :: Value -> AssetClass -> Bool
isSingletonOf v a = case flattenValue v of 
    [(c, t, _)] -> (c == c') && (t == t')
    _           -> False
  where 
    c' = fst $ unAssetClass a
    t' = snd $ unAssetClass a

{-# INLINABLE unSingleton #-}
unSingleton :: Value -> Maybe (AssetClass, Integer)
unSingleton v  = case flattenValue v of 
        [(c, t, a)] -> Just (assetClass c t, a)
        _           -> Nothing

assetClassSingleton :: AssetClass -> Integer -> Value 
assetClassSingleton a = V.singleton c t 
  where 
    c = fst $ unAssetClass a
    t = snd $ unAssetClass a

-- TODO replaced pool :: Set AssetClass with List for simplicity; 
-- deduplication and sorting are checked in packLiquidityCoins. Double-check for exploits

-- symbolizing percentual ownership of one coin in one pool
data LiquidityCoin = LiquidityCoin
                   { pool :: LiquidityPool 
                   , coin :: AssetClass
                   }
                deriving (Haskell.Show, Generic, ToJSON, FromJSON)
                --    deriving (Haskell.Show, Haskell.Read, Generic, ToJSON, FromJSON, ToSchema)
PlutusTx.makeIsDataIndexed ''LiquidityCoin [('LiquidityCoin, 0)]
PlutusTx.makeLift ''LiquidityCoin


-- TODO check everywhere for things like "PlutusTx.Map.fromList does not deduplicate keys"

-- TODO data types to reflect weights matter/don't matter
data MirqurAction   = Create    LiquidityPool -- weights matter    
                    | Revive    LiquidityPool -- weights matter 
                    | Drain     LiquidityPool -- weights don't matter
                    | Swap      LiquidityPool AssetClass -- AssetClass is the output; weights don't matter
                    | Remove    LiquidityPool Value -- weights don't matter
                    | Add       LiquidityPool -- weights don't matter
    deriving Haskell.Show
PlutusTx.makeIsDataIndexed ''MirqurAction [ ('Create,  0)
                                           , ('Revive, 1)
                                           , ('Drain,  2)
                                           , ('Swap,   3)
                                           , ('Remove, 4)
                                           , ('Add,    5)
                                           ]
PlutusTx.makeLift ''MirqurAction

{-# INLINABLE getBatchingLP #-}
getBatchingLP :: MirqurAction -> Maybe LiquidityPool
getBatchingLP a = case a of 
    Create    _     -> Nothing 
    Revive    lp    -> Just lp 
    Drain     lp    -> Just lp
    Swap      lp _  -> Just lp
    Remove    lp _  -> Just lp
    Add       lp    -> Just lp

-- we carry along the last hash to chain them
type LastHash = BuiltinByteString

data MirqurDatum =
      Factory [LiquidityPool]
    | Pool LiquidityPool UnpackedLiquidity LastHash
    | Action MirqurAction Address
    deriving stock (Haskell.Show)
PlutusTx.makeIsDataIndexed ''MirqurDatum [ ('Factory,  0)
                                          , ('Pool,    1)
                                          , ('Action,  2)
                                          ]
PlutusTx.makeLift ''MirqurDatum

data MirqurRedeemer   = CreateInFactory LiquidityPool -- weights matter    
                      | BatchPoolActions  
                      | BatchAction
    deriving Haskell.Show
PlutusTx.makeIsDataIndexed ''MirqurRedeemer [ ('CreateInFactory,  0)
                                            , ('BatchPoolActions, 1)
                                            , ('BatchAction,  2)
                                            ]
PlutusTx.makeLift ''MirqurRedeemer

data ActionInfo = ActionInfo
                { infoInValue       :: Value 
                , infoActionType    :: MirqurAction
                , infoOutValue      :: Value
                , infoRank          :: BuiltinByteString -- for sorting
                }
PlutusTx.makeIsDataIndexed ''ActionInfo [ ('ActionInfo,  0)]
PlutusTx.makeLift ''ActionInfo

instance Eq ActionInfo where 
    {-# INLINABLE (==) #-}
    (==) a b = (infoRank a) == (infoRank b)

instance Ord ActionInfo where 
    {-# INLINABLE (<) #-}
    (<) a b = (infoRank a) < (infoRank b)
    {-# INLINABLE (<=) #-}
    (<=) a b = (infoRank a) <=(infoRank b)
    {-# INLINABLE (>) #-}
    (>) a b = (infoRank a) > (infoRank b)
    {-# INLINABLE (>=) #-}
    (>=) a b = (infoRank a) >= (infoRank b)

data ActionRaw = ActionRaw 
                { rawActionType    :: MirqurAction
                , rawUserAddress   :: Address
                , rawTxoRef        :: TxOutRef
                , rawTxoTx         :: TxOutTx
                , rawRank          :: BuiltinByteString -- for sorting
                }

instance Eq ActionRaw where 
    (==) a b = (rawRank a) == (rawRank b)

instance Ord ActionRaw where 
    (<) a b = (rawRank a) < (rawRank b)
    (<=) a b = (rawRank a) <=(rawRank b)
    (>) a b = (rawRank a) > (rawRank b)
    (>=) a b = (rawRank a) >= (rawRank b)

