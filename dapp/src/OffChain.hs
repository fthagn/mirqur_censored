{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module OffChain -- TODO uncomment
    -- ( poolStateCoinFromMirqurCurrency--, liquidityCoin
    -- , OpenParams (..)
    -- , SwapParams (..)
    -- , DrainParams (..)
    -- , RemoveParams (..)
    -- , AddParams (..)
    -- , MirqurUserSchema, UserContractState (..)
    -- , MirqurOwnerSchema
    -- , start, open, add, remove, drain, swap, pools
    -- , ownerEndpoint, userEndpoints
    -- ) where
  where

import           Control.Monad                    hiding (fmap)
import qualified Data.Map                         as Map
import           Data.Monoid                      (Last (..))
import           Data.Proxy                       (Proxy (..))
import           Data.Text                        (Text, pack)
import           Data.Void                        (Void, absurd)
import           Ledger                           hiding (singleton)
import           Ledger.Constraints               as Constraints
import qualified Ledger.Typed.Scripts             as Scripts
import           Playground.Contract
import           Plutus.Contract
import qualified Plutus.Contracts.Currency        as Currency
import           OnChain (mkMirqurValidator, validateLiquidityMinting)
import           Pool
import           Types
import qualified PlutusTx
import           PlutusTx.Prelude                 hiding (Semigroup (..), dropWhile, flip, unless)
import           Prelude                          as Haskell (Int, Semigroup (..), String, div, dropWhile, flip, show,
                                                              (^), foldr1, map) -- foldr1 and after added
import           Text.Printf                      (printf)

-- new imports
import qualified Ledger.Value                  as V
import           Ledger.Credential             (Credential(..))

data MirqurDEX
instance Scripts.ValidatorTypes MirqurDEX where
    type instance RedeemerType MirqurDEX = MirqurRedeemer
    type instance DatumType    MirqurDEX = MirqurDatum

type MirqurOwnerSchema = Endpoint "start" ()

-- | Schema for the endpoints for users of Mirqur.
type MirqurUserSchema =
            Endpoint "open"   OpenParams
        .\/ Endpoint "swap"   SwapParams
        .\/ Endpoint "drain"  DrainParams
        .\/ Endpoint "remove" RemoveParams
        .\/ Endpoint "add"    AddParams
        .\/ Endpoint "pools"  ()
        .\/ Endpoint "funds"  ()
        .\/ Endpoint "stop"   ()

-- | Type of the Mirqur user contract state.
data UserContractState =
      Pools [(LiquidityPool, UnpackedLiquidity, LastHash)] 
    | Funds Value
    | Opened
    | Swapped
    | Added
    | Removed
    | Drained
    | Stopped
    deriving (Show, Generic, FromJSON, ToJSON)


mirqurTokenName, poolStateTokenName :: TokenName
mirqurTokenName = "Mirqur"
poolStateTokenName = "Pool State"

mirqurInstance :: Mirqur -> Scripts.TypedValidator MirqurDEX
mirqurInstance mq = Scripts.mkTypedValidator @MirqurDEX
    ($$(PlutusTx.compile [|| mkMirqurValidator ||])
        `PlutusTx.applyCode` PlutusTx.liftCode mq
        `PlutusTx.applyCode` PlutusTx.liftCode c)
     $$(PlutusTx.compile [|| wrap ||])
  where
    c :: Coin PoolState
    c = poolStateCoin mq

    wrap = Scripts.wrapValidator @MirqurDatum @MirqurRedeemer

mirqurScript :: Mirqur -> Validator
mirqurScript = Scripts.validatorScript . mirqurInstance

mirqurAddress :: Mirqur -> Ledger.Address
mirqurAddress = Ledger.scriptAddress . mirqurScript

mirqur :: CurrencySymbol -> Mirqur
mirqur cs = Mirqur $ mkCoin cs mirqurTokenName

liquidityPolicy :: Mirqur -> MintingPolicy
liquidityPolicy mq = mkMintingPolicyScript $
    $$(PlutusTx.compile [|| \u t -> Scripts.wrapMintingPolicy (validateLiquidityMinting u t) ||])
        `PlutusTx.applyCode` PlutusTx.liftCode mq
        `PlutusTx.applyCode` PlutusTx.liftCode poolStateTokenName

liquidityCurrency :: Mirqur -> CurrencySymbol
liquidityCurrency = scriptCurrencySymbol . liquidityPolicy

poolStateCoin :: Mirqur -> Coin PoolState
poolStateCoin = flip mkCoin poolStateTokenName . liquidityCurrency

-- | Gets the 'Coin' used to identity liquidity pools.
poolStateCoinFromMirqurCurrency :: CurrencySymbol -- ^ The currency identifying the Mirqur instance.
                                 -> Coin PoolState
poolStateCoinFromMirqurCurrency = poolStateCoin . mirqur

-- | Gets the liquidity token for a given liquidity pool.
-- liquidityCoin :: CurrencySymbol -- ^ The currency identifying the Mirqur instance.
--               -> Coin A         -- ^ One coin in the liquidity pair.
--               -> Coin B         -- ^ The other coin in the liquidity pair.
--               -> Coin Liquidity
-- liquidityCoin cs coinA coinB = mkCoin (liquidityCurrency $ mirqur cs) $ lpTicker $ LiquidityPool coinA coinB

-- raw variants because we don't want to implement >9k schemas,
-- and need to translate at some point anyways 
-- TODO check at some point if it might be better to use schema or json
type LPID = [AssetClass]
type V = [(AssetClass, Integer)]

-- TODO revert dummy
parseLPID :: LPID -> LiquidityPool
parseLPID l = LiquidityPool $ Weights $ Haskell.foldr1 (<>) $ Haskell.map (\x -> V.assetClassValue x 1) l
-- parseLPID l = LiquidityPool $ Haskell.foldr1 (rUnionWith (+)) $ Haskell.map (\x -> rAssetClassValue x (fromInteger 1)) l

parseV :: V -> Value 
parseV v = foldr1 (<>) $ Haskell.map (\(x, y) -> (V.assetClassValue x y)) v

negateV :: V -> V 
negateV = Haskell.map (\(asset, amt) -> (asset, negate amt))

-- | Parameters for the @open@-endpoint, which creates a new liquidity pool.
data OpenParams = OpenParams
    { openWeights   :: V--RValue
    , openBalances   :: V--Value
    } deriving (Show, Generic, ToJSON, FromJSON, ToSchema)

-- | Parameters for the @swap@-endpoint, which allows swaps between the two different coins in a liquidity pool.
-- One of the provided amounts must be positive, the other must be zero.
data SwapParams = SwapParams
    { swapPool        :: LPID -- LiquidityPool
    , inDiff       :: Integer     
    , inAsset   :: AssetClass         -- ^ One 'Coin' of the liquidity pair.
    , outAsset   :: AssetClass         -- ^ The other 'Coin'.  -- ^ The amount the first 'Coin' that should be swapped.
    } deriving (Show, Generic, ToJSON, FromJSON, ToSchema)

-- | Parameters for the @drain@-endpoint, which drains a liquidity pool.
data DrainParams = DrainParams
    { drainPool :: LPID --LiquidityPool -- weights are irrelevant here because == only checks that coins align
    } deriving (Show, Generic, ToJSON, FromJSON, ToSchema)

-- | Parameters for the @remove@-endpoint, which removes some liquidity from a liquidity pool.
data RemoveParams = RemoveParams
    { removePool :: LPID -- LiquidityPool -- weights are irrelevant here because == only checks that coins align
    , removeLiquidity  :: V
    } deriving (Show, Generic, ToJSON, FromJSON, ToSchema)

-- | Parameters for the @add@-endpoint, which adds liquidity to a liquidity pool in exchange for liquidity tokens.
data AddParams = AddParams
    { addPool :: LPID -- LiquidityPool -- the pool to add to. Note that this has contain the balances-coins but may have more
    , addBalances :: V--Value -- the coin-balances to add
    } deriving (Show, Generic, ToJSON, FromJSON, ToSchema)

data BatchingState = BatchingState 
                    { vps     :: VirtualPoolState
                    , lookups :: ScriptLookups MirqurDEX
                    , tx      :: TxConstraints (Scripts.RedeemerType MirqurDEX) (Scripts.DatumType MirqurDEX)
                    , log     :: BuiltinString
                    }

-- | Creates a Mirqur "factory". This factory will keep track of the existing liquidity pools and enforce that there will be at most one liquidity pool
-- for any pair of tokens at any given time.
start :: forall w s. Contract w s Text Mirqur
start = do
    pkh <- pubKeyHash <$> ownPubKey
    cs  <- fmap Currency.currencySymbol $
           mapError (pack . show @Currency.CurrencyError) $
           Currency.mintContract pkh [(mirqurTokenName, 1)]
    let c    = mkCoin cs mirqurTokenName
        mq   = mirqur cs
        inst = mirqurInstance mq
        tx   = mustPayToTheScript (Factory []) $ unitValue c
    ledgerTx <- submitTxConstraints inst tx
    void $ awaitTxConfirmed $ txId ledgerTx
    void $ waitNSlots 1

    logInfo @String $ printf "started Mirqur %s at address %s" (show mq) (show $ mirqurAddress mq)
    return mq

-- TODO probably need some ADA in the pending actions too for sandstorm attack defense

includeRevive :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
              -> Value -> Weights -> VirtualPoolState -> Either BuiltinString (Value, VirtualPoolState)
includeRevive packLCs inV inW vps@VirtualPoolState{..}
    | virtualBalances /= zeroVal                = Left "pool not empty\n--> revival not included"
    | not (inV `sameCoins` (unwrapWeights inW)) = Left "deposit not matching pool\n--> revival not included"
    | otherwise = let
            newL = calculateInitialLiquidity inV
            packed = unwrapPacked $ checkErrors $ packLCs (Right newL)
            
        in 
            Right (packed, newVPS)

includeDrain :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
              -> Value -> VirtualPoolState -> Either BuiltinString (Value, VirtualPoolState)
includeDrain packLCs inV vps@VirtualPoolState{..}
    | inV /= oldL   = Left "incorrect liquidity tokens\n--> ndraining not included"
    | otherwise     = Right (virtualBalances, newVPS)
  where
    oldL = unwrapPacked $ checkErrors $ packLCs $ Right virtualLiquidity
    zeroL = UnpackedLiquidity zeroVal
   

includeSwap :: Value -> AssetClass -> VirtualPoolState -> Either BuiltinString (Value, VirtualPoolState)
includeSwap inV outAsset vps@VirtualPoolState{..}
  | virtualBalances == zeroVal = Left "pool is empty\n--> swap not included"--(inV, vps) -- i.e. if a previous action drained the pool return funds
  | s == Nothing = Left "swap requires exactly one input-asset\n--> swap not included"
  | isLeft outDiff = Left $ fromLeft' outDiff `appendString` "\n--> swap not included"
  | otherwise = Right (outV, vps{virtualBalances = newB})
  where
    s = unSingleton inV
    Just (inAsset, inDiff) = s -- TODO does this work?
    outV = assetClassSingleton outAsset $ checkErrors outDiff -- TODO does this work?
    newB = virtualBalances <> inV <> (negate outV)


-- TODO catch errors or refactor safeV*
includeRemove :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
              -> Value -> Value -> VirtualPoolState -> Either BuiltinString (Value, VirtualPoolState)
includeRemove packLCs inV outIdeal vps@VirtualPoolState{..} = let 
    oldB = Right virtualBalances
    oldL = Right virtualLiquidity
    newB = oldB `safeVsub` (Right outIdeal)
    newL = calculateNewLiquidity oldB virtualLiquidity newB -- checks for asset alignment
    diffL = oldL `safeLsub` newL
    packed = unwrapETPacked $ packLCs diffL 
    retour = passIf nonnegative $ (Right inV) `safeVsub` packed
   in 

includeAdd :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
              -> Value -> VirtualPoolState -> Either BuiltinString (Value, VirtualPoolState)
includeAdd packLCs inV vps@VirtualPoolState{..}
  | virtualBalances == zeroVal = Left "adding to empty pool, use 'revive' here instead" -- i.e. if a previous action drained the pool return funds
  | otherwise = let
    oldB = Right virtualBalances
    newB = oldB `safeVadd` (Right inV) -- prohibits reviving (balance: 0 --> pos)
    newL = calculateNewLiquidity oldB virtualLiquidity newB -- checks for asset alignment
    diffL = newL `safeLsub` (Right virtualLiquidity) -- prohibits addition of unlisted assets
    packed = unwrapETPacked $ packLCs diffL 
    in


includeAction :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
                 -> ActionRaw -> BatchingState -> BatchingState
includeAction packLCs ActionRaw{..} bs@BatchingState{..} = case processed of 
    Left l               -> bs{log = log `appendString` "\n" `appendString` l} -- TODO does this work?
    Right (outV, newVPS) -> BatchingState 
                            { vps     = newVPS
                            , lookups = newLookups
                            , tx      = newTx outV (LiquidityPool $ virtualWeights newVPS)
                            , log     = log `appendString` "\n--> action included" -- TODO does this work?
                            }
  where 
    inV = txOutValue $ txOutTxOut rawTxoTx
    processed = case rawActionType of 
                    Revive  (LiquidityPool inW) -> includeRevive packLCs inV inW      vps
                    Drain   _                   -> includeDrain  packLCs inV          vps
                    Swap    _ outAsset          -> includeSwap           inV outAsset vps
                    Remove  _ outValue          -> includeRemove packLCs inV outValue vps
                    Add     _                   -> includeAdd    packLCs inV          vps
                    _                           -> Left "error decoding action info"

    newLookups = lookups <> Constraints.unspentOutputs (Map.singleton rawTxoRef rawTxoTx)

    redeemer = Redeemer $ PlutusTx.toBuiltinData BatchAction
    inConstraint = Constraints.mustSpendScriptOutput rawTxoRef redeemer
    outConstraint outV lp = case (addressCredential rawUserAddress) of 
        PubKeyCredential pubKeyHash       -> Constraints.mustPayToPubKey 
                                                         pubKeyHash 
                                                         outV
        ScriptCredential validatorHash    -> Constraints.mustPayToOtherScript 
                                                         validatorHash
                                                         (Datum $ PlutusTx.toBuiltinData lp) -- TODO just using current pool here, probably not the most useful option
                                                         outV 
        
    newTx outV lp = tx <> inConstraint <> (outConstraint outV lp)

    
-- 1. get pending actions (excluding Create) at pool
-- 2. for all: calculate rank
-- 3. sort by rank
-- 4. fold them, updating vps as well as collecting lookups and tx
-- 5. execute with final vps, lookups and tx
batch :: forall w s. Mirqur -> LiquidityPool -> Contract w s Text () -- TODO signature?
batch mq lp' = do 
    (_, (lpTxoref, lpTxotx, (lp, lq, lh))) <- findMirqurFactoryAndPool mq lp' -- gets current lp state and double-checks with factory
    (actionsRaw :: [ActionRaw])            <- findBatchingActionsAtPool mq lh lp'
    pkh                                    <- pubKeyHash <$> ownPubKey
    let addr                                = pubKeyHashAddress pkh

    if   not $ any (\a -> rawUserAddress a == addr) actionsRaw
    then batch mq lp' -- TODO delay?
    else do
        let actionsSorted = mergesort actionsRaw -- same sorting-function as onchain
            psC           = poolStateCoin mq 
            unitPSC       = unitValue psC
            mqInst        = mirqurInstance mq
            mqScript      = mirqurScript mq

            redeemer = Redeemer $ PlutusTx.toBuiltinData BatchPoolActions

            initBatchingState = BatchingState 
                        { vps     = initVPS
                        , lookups = Constraints.typedValidatorLookups mqInst        <>
                                    Constraints.otherScript mqScript                <>
                                    Constraints.unspentOutputs (Map.singleton lpTxoref lpTxotx)
                        , tx      = Constraints.mustSpendScriptOutput lpTxoref redeemer
                        , log     = "----> begin batching"
                        }

            packLCs            = packLiquidityCoins psC lp
            finalBatchingState = foldr (includeAction packLCs) initBatchingState actionsSorted
            finalVPS           = vps finalBatchingState
            mintingValue       = tallyMint $ finalVPS

            newPool      = LiquidityPool $ virtualWeights finalVPS
            newLiquidity = virtualLiquidity finalVPS
            newHash      = chainedVPSHash lh finalVPS
            newDatum     = Pool newPool newLiquidity newHash
            newBalances  = virtualBalances finalVPS

            batchedLookups = lookups finalBatchingState
            batchedTx      = tx finalBatchingState   <>
                            Constraints.mustPayToTheScript newDatum (newBalances <> unitPSC) 

            (mintedLookups, mintedTx)
                | mintingValue == zeroVal = (batchedLookups, batchedTx)
                | otherwise               = ( batchedLookups  <> 
                                            Constraints.mintingPolicy (liquidityPolicy mq)
                                            , batchedTx   <>
                                            Constraints.mustMintValue mintingValue )


        logInfo $ fromBuiltin $ log finalBatchingState
        logInfo @String "----> finished batching\nsubmitting"

        ledgerTx <- submitTxConstraintsWith mintedLookups mintedTx
        void $ awaitTxConfirmed $ txId ledgerTx

        logInfo $ "batched actions for: " ++ show lp


-- | Creates a liquidity pool for a pair of coins. The creator provides liquidity for both coins and gets liquidity tokens in return.
open :: forall w s. Mirqur -> OpenParams -> Contract w s Text ()
open mq OpenParams{..} = do
    let w  = parseV openWeights
        b  = parseV openBalances
        lp = LiquidityPool $ reduceWeights $ Weights w

    when (length (V.flattenValue w) < 2) $ throwError "needs at least two coins"
    when (not (sameCoins w b)) $ throwError "weights and initial balances not aligned"
    (oref, o, lps) <- findMirqurFactory mq

    if   lp `elem` lps
    then submit mq (Revive lp) b
    else do -- TODO do needed?
        let psC       = poolStateCoin mq
            initL     = calculateInitialLiquidity b
            packed    = checkErrors $ packLiquidityCoins psC lp (Right initL) -- inital emission of liquidity coins just equals initial balances 1:1

            mqInst    = mirqurInstance mq
            mqScript  = mirqurScript mq

            mqDat     = Factory $ lp : lps
            mqVal     = unitValue $ mqCoin mq

            lpDat     = Pool lp initL (vpsHash initVPS)
            lpVal     = b <> unitValue psC
            
            redeemer  = Redeemer $ PlutusTx.toBuiltinData $ Create lp

        let lookups  = Constraints.typedValidatorLookups mqInst       <>
                    Constraints.otherScript mqScript               <>
                    Constraints.mintingPolicy (liquidityPolicy mq) <>
                    Constraints.unspentOutputs (Map.singleton oref o)

        let tx       = Constraints.mustPayToTheScript mqDat mqVal                      <>
                    Constraints.mustPayToTheScript lpDat lpVal                         <>
                    Constraints.mustMintValue (unitValue psC <> (unwrapPacked packed)) <>
                    Constraints.mustSpendScriptOutput oref redeemer

        ledgerTx <- submitTxConstraintsWith lookups tx

        void $ awaitTxConfirmed $ txId ledgerTx

        logInfo $ "created liquidity pool: " ++ show lp

drain :: forall w s. Mirqur -> DrainParams -> Contract w s Text ()
drain mq DrainParams{..} = do
    pkh <- pubKeyHash <$> ownPubKey
    let addr      = pubKeyHashAddress pkh
        c         = poolStateCoin mq
        lp        = parseLPID drainPool
        draining  = Drain lp
        packedRaw = unwrapPacked 
                  $ checkErrors 
                  $ packLiquidityCoins c lp
                  $ Right
                  $ UnpackedLiquidity 
                  $ unwrapWeights 
                  $ weights lp -- TODO does this work?

    utxos <- utxoAt addr
    let packedMax = totalAssetsLike packedRaw utxos

    submit mq draining packedMax

swap :: forall w s. Mirqur -> SwapParams -> Contract w s Text ()
swap mq SwapParams{..} = do
    let lp       = parseLPID swapPool
        swapping = Swap lp outAsset
        inV      = assetClassSingleton inAsset inDiff

    submit mq swapping inV

remove :: forall w s. Mirqur -> RemoveParams -> Contract w s Text ()
remove mq RemoveParams{..} = do
    pkh <- pubKeyHash <$> ownPubKey
    let addr      = pubKeyHashAddress pkh
        c         = poolStateCoin mq
        lp        = parseLPID removePool
        outV      = parseV removeLiquidity
        removing  = Remove lp outV
        packedRaw = unwrapPacked 
                  $ checkErrors 
                  $ packLiquidityCoins c lp
                  $ Right
                  $ UnpackedLiquidity outV

    utxos <- utxoAt addr
    let packedMax = totalAssetsLike packedRaw utxos

    if   packedRaw `V.leq` packedMax
    then submit mq removing packedMax
    else throwError "insufficient liquidity coins for removal"

add :: forall w s. Mirqur -> AddParams -> Contract w s Text ()
add mq AddParams{..} = do
    let lp       = parseLPID addPool 
        adding   = Add lp
        inV      = parseV addBalances

    submit mq adding inV

-- TODO better to pass lp as separate argument or extract from action again?
submit :: forall w s. Mirqur -> MirqurAction -> Value -> Contract w s Text ()
submit mq action inV = do
    pkh <- pubKeyHash <$> ownPubKey
    
    let mqInst     = mirqurInstance mq
        mqScript   = mirqurScript mq
        lp         = fromMaybe (error ()) (getBatchingLP action) -- TODO does this work?

        dat        = Action action $ pubKeyHashAddress pkh

    let lookups  = Constraints.typedValidatorLookups mqInst       <>
                   Constraints.otherScript mqScript               <>
                   Constraints.ownPubKeyHash pkh

    let tx       = Constraints.mustPayToTheScript dat inV

    ledgerTx <- submitTxConstraintsWith lookups tx
    void $ awaitTxConfirmed $ txId ledgerTx
    logInfo @String "action submitted"
    batch mq lp


-- | Finds all liquidity pools and their liquidity belonging to the Mirqur instance.
-- This merely inspects the blockchain and does not issue any transactions.
pools :: forall w s. Mirqur -> Contract w s Text [(LiquidityPool, UnpackedLiquidity, LastHash)]
pools mq = do
    utxos <- utxoAt (mirqurAddress mq)
    go $ snd <$> Map.toList utxos
  where
    go :: [TxOutTx] -> Contract w s Text [(LiquidityPool, UnpackedLiquidity, LastHash)]
    go []       = return []
    go (o : os) = do
        let v = txOutValue $ txOutTxOut o
        if isUnity v c
            then do
                d <- getMirqurDatum o
                case d of
                    Factory _ -> go os
                    Pool lp lq lh -> do
                        let s   = (lp, lq, lh)
                        logInfo $ "found pool: " ++ show s
                        ss <- go os
                        return $ s : ss
            else go os
      where
        c :: Coin PoolState
        c = poolStateCoin mq

-- | Gets the caller's funds.
funds :: forall w s. Contract w s Text Value
funds = do
    pkh <- pubKeyHash <$> ownPubKey
    os  <- Haskell.map snd . Map.toList <$> utxoAt (pubKeyHashAddress pkh)
    return $ mconcat [txOutValue $ txOutTxOut o | o <- os]

-- | gets a value of all balances of the coins in the first input value
totalAssetsLike :: Value -> Map.Map TxOutRef TxOutTx -> Value
totalAssetsLike v utxos = 
    foldr1' (<>) $ likeV . txOutValue . txOutTxOut . snd <$> Map.toList utxos
  where 
    
    likeI :: Integer -> Integer -> Integer 
    likeI 0 _ = 0
    likeI _ i = i

    likeV :: Value -> Value
    likeV v' = V.unionWith likeI v v'

maybeGetAction :: TxOutTx -> Maybe (MirqurAction, Address)
maybeGetAction o = case txOutDatumHash $ txOutTxOut o of
        Just h             -> case Map.lookup h $ txData $ txOutTxTx o of
            Just (Datum e) -> case PlutusTx.fromBuiltinData e of 
                Just (Action action addr) -> Just (action, addr)
                _          -> Nothing
            _              -> Nothing
        _                  -> Nothing
    
getMirqurDatum :: TxOutTx -> Contract w s Text MirqurDatum
getMirqurDatum o = case txOutDatumHash $ txOutTxOut o of
        Nothing -> throwError "datumHash not found"
        Just h -> case Map.lookup h $ txData $ txOutTxTx o of
            Nothing -> throwError "datum not found"
            Just (Datum e) -> case PlutusTx.fromBuiltinData e of
                Nothing -> throwError "datum has wrong type"
                Just d  -> return d

findMirqurInstance :: forall a b w s. Mirqur -> Coin b -> (MirqurDatum -> Maybe a) -> Contract w s Text (TxOutRef, TxOutTx, a)
findMirqurInstance mq c f = do
    let addr = mirqurAddress mq
    logInfo @String $ printf "looking for Mirqur instance at address %s containing coin %s " (show addr) (show c)
    utxos <- utxoAt addr
    go  [x | x@(_, o) <- Map.toList utxos, isUnity (txOutValue $ txOutTxOut o) c]
  where
    go [] = do
        throwError "Mirqur instance not found"
    go ((oref, o) : xs) = do
        logInfo @String "getting MirqurDatum"
        d <- getMirqurDatum o
        case f d of
            Nothing -> go xs
            Just a  -> do
                logInfo @String $ printf "found Mirqur instance with datum: %s" (show d)
                return (oref, o, a)

findMirqurFactory :: forall w s. Mirqur -> Contract w s Text (TxOutRef, TxOutTx, [LiquidityPool])
findMirqurFactory mq@Mirqur{..} = findMirqurInstance mq mqCoin $ \case
    Factory lps -> Just lps
    Pool    {}  -> Nothing


findMirqurPool :: forall w s. Mirqur -> LiquidityPool -> Contract w s Text (TxOutRef, TxOutTx, (LiquidityPool, UnpackedLiquidity, LastHash))
findMirqurPool mq lp' = findMirqurInstance mq (poolStateCoin mq) $ \case
        Pool lp lq lh
            | lp == lp' -> Just (lp, lq, lh) -- NOTE: it's important to return lp over lp' here; the former only has the current weights for certain
        _               -> Nothing

-- TODO this seems overly convoluted, since factory is never used downstream and pools are identified by pooltokens already
-- find factory and pool with exactly those tokens
findMirqurFactoryAndPool :: forall w s. 
                          Mirqur
                          -> LiquidityPool
                          -> Contract w s Text ( (TxOutRef, TxOutTx, [LiquidityPool])
                                               , (TxOutRef, TxOutTx, (LiquidityPool, UnpackedLiquidity, LastHash))
                                               )
findMirqurFactoryAndPool mq lp = do
    (oref1, o1, lps) <- findMirqurFactory mq
    case [ lp'
         | lp' <- lps
         , lp' == lp -- any nonzero weights work here
         ] of
        [re] -> do
            (oref2, o2, a) <- findMirqurPool mq re -- NOTE it's important to return current weights, not the ones the pool registered with (which findUniswalPool does)
            return ( (oref1, o1, lps)
                   , (oref2, o2, a)
                   )
        _    -> throwError "liquidity pool not found"

findBatchingActionsAtPool :: forall w s. Mirqur -> LastHash -> LiquidityPool -> Contract w s Text [ActionRaw]--[(TxOutRef, TxOutTx, MirqurAction, Address)]
findBatchingActionsAtPool mq lh lp = do 
    let addr = mirqurAddress mq
    utxos <- utxoAt addr
    return [ ActionRaw 
                { rawActionType     = action
                , rawUserAddress    = addr
                , rawTxoRef         = txor
                , rawTxoTx          = txotx
                , rawRank           = chainedAddressHash lh addr
                }--(txor, txotx, action, addr)
           | (txor, txotx) <- Map.toList utxos -- (TxOutRef, TxOutTx)
           , let datum = maybeGetAction txotx
           , isJust datum
           , let Just (action, addr) = datum 
           , fitsPoolAndType lp action
           ]

findSwap :: Value -> Weights -> AssetClass -> Integer -> AssetClass -> Either BuiltinString Integer
findSwap bal (Weights weights) inAsset inDiff outAsset
    | ub' <= 1    = Left "Swap returns 0"
    | otherwise  = go 1 ub'
  where 
    



ownerEndpoint :: Contract (Last (Either Text Mirqur)) EmptySchema ContractError ()
ownerEndpoint = do
    e <- mapError absurd $ runError start
    void $ waitNSlots 1
    tell $ Last $ Just e

-- | Provides the following endpoints for users of a Mirqur instance:
--
--      [@open@]: Creates a liquidity pool for a pair of coins. The creator provides liquidity for both coins and gets liquidity tokens in return.
--      [@swap@]: Uses a liquidity pool two swap one sort of coins in the pool against the other.
--      [@drain@]: Drains a liquidity pool by burning all remaining liquidity tokens in exchange for all liquidity remaining in the pool.
--      [@remove@]: Removes some liquidity from a liquidity pool in exchange for liquidity tokens.
--      [@add@]: Adds some liquidity to an existing liquidity pool in exchange for newly minted liquidity tokens.
--      [@pools@]: Finds all liquidity pools and their liquidity belonging to the Mirqur instance. This merely inspects the blockchain and does not issue any transactions.
--      [@funds@]: Gets the caller's funds. This merely inspects the blockchain and does not issue any transactions.
--      [@stop@]: Stops the contract.
userEndpoints :: Mirqur -> Promise (Last (Either Text UserContractState)) MirqurUserSchema Void ()
userEndpoints mq =
    stop
        `select`
    (void (f (Proxy @"open")   (const Opened)  open                   `select`
           f (Proxy @"swap")   (const Swapped) swap                   `select`
           f (Proxy @"drain")  (const Drained)  drain                 `select`
           f (Proxy @"remove") (const Removed) remove                 `select`
           f (Proxy @"add")    (const Added)   add                    `select`
           f (Proxy @"pools")  Pools           (\mq' () -> pools mq') `select`
           f (Proxy @"funds")  Funds           (\_mq () -> funds))
     <> userEndpoints mq)
  where
    f :: forall l a p.
         (HasEndpoint l p MirqurUserSchema, FromJSON p)
      => Proxy l
      -> (a -> UserContractState)
      -> (Mirqur -> p -> Contract (Last (Either Text UserContractState)) MirqurUserSchema Text a)
      -> Promise (Last (Either Text UserContractState)) MirqurUserSchema Void ()
    f _ g c = handleEndpoint @l $ \p -> do
        e <- either (pure . Left) (runError . c mq) p
        tell $ Last $ Just $ case e of
            Left err -> Left err
            Right a  -> Right $ g a

    stop :: Promise (Last (Either Text UserContractState)) MirqurUserSchema Void ()
    stop = handleEndpoint @"stop" $ \e -> do
        tell $ Last $ Just $ case e of
            Left err -> Left err
            Right () -> Right Stopped
