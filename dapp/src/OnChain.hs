{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

{-# LANGUAGE NamedFieldPuns             #-} -- added

{-# options_ghc -fno-strictness         #-}
{-# options_ghc -fno-specialise         #-}



module OnChain
    ( mkMirqurValidator
    , validateLiquidityMinting
    ) where

import           Ledger
import           Ledger.Constraints.OnChain       as Constraints
import           Ledger.Constraints.TxConstraints as Constraints
import           Ledger.Value                     -- TODO uncomment (AssetClass (..), symbols)
import           Pool   -- (calculateAdditionalLiquidity,
                                                 --  calculateRemoval, checkSwap, packLiquidityCoins)
import           Types
import qualified PlutusTx
import           PlutusTx.Prelude

{-# INLINABLE findOwnInput' #-}
findOwnInput' :: ScriptContext -> TxInInfo
findOwnInput' ctx = fromMaybe (error ()) (findOwnInput ctx)

{-# INLINABLE valueWithin #-}
valueWithin :: TxInInfo -> Value
valueWithin = txOutValue . txInInfoResolved

{-# INLINABLE validateCreate #-}
-- | Ths validates the creation of a liquidity pool to exchange coins. In order to be
-- valid,
--
--  1,2. We need to be dealing with the Mirqur coin,
--  3. We have to exchanging multiple coins,
--  4. The pool can't already exist,
--  5. The pool needs a single pool-coin as output, newly minted
--  6. The liquidity-coin amounts need to match the deposit
--  7. inputs and weights must be same coins
--  8. The pool-record needs be correct
--  9. The recorded liquidities in pool-datum need be correct


    -- data OutputConstraint a =
    -- OutputConstraint
    --     { ocDatum :: a
    --     , ocValue :: Value
    --     } deriving stock (Haskell.Show, Generic, Haskell.Functor)       

-- TODO check somewhere that Mirqur and Coin Poolstate match, maybe? nonessential but safer 
validateCreate :: Mirqur
               -> Coin PoolState
               -> [LiquidityPool]
               -> LiquidityPool
               -> ScriptContext
               -> Bool
validateCreate Mirqur{..} c lps lp@LiquidityPool{..} ctx =
    traceIfFalse "Mirqur coin not present"      (isUnity (valueWithin $ findOwnInput' ctx) mqCoin)   && -- 1.
    traceIfFalse "Factory output wrong"         factoryOutputCorrect                                && -- 2.
    traceIfFalse "too few coins in pool"        (length (flattenValue (unwrapWeights weights)) > 1) && -- 3.
    traceIfFalse "Pool exists"                  (all (/= lp) lps)                                   && -- 4.
    traceIfFalse "incorrect minting"            (minted == mintedIdeal)                             && -- 5. 6. 
    traceIfFalse "weights and coins mismatch"   (sameCoins (unwrapWeights weights) initBal)         && -- 7.        
    traceIfFalse "Datum wrong"                  datumCorrect                                        && -- 8. 9.
    traceIfFalse "reduce weights please"        (weights == (reduceWeights weights)) -- TODO could be abstracted                    

  where
    factoryOutputCorrect :: Bool 
    factoryOutputCorrect = Constraints.checkOwnOutputConstraint ctx 
                           (OutputConstraint (Factory $ lp : lps) $ unitValue mqCoin)

    poolOutput :: TxOut
    poolOutput = case (getContinuingOutputs ctx) of 
        [o] -> o
        _   -> traceError "expected exactly one pool output"
  
    initVal, initBal :: Value 
    initVal = txOutValue poolOutput 
    initBal = checkErrors $ (Right initVal) `safeVsub` (Right $ unitValue c)  -- 5. (saveVsub checks that the latter is in the former)
    initLiq = calculateInitialLiquidity initBal

    info :: TxInfo
    info = scriptContextTxInfo ctx

    minted :: Value
    minted = txInfoMint info

    mintedIdeal :: Value 
    mintedIdeal = unitValue c <> (unwrapPacked $ checkErrors $ packLiquidityCoins c lp $ Right initLiq)


    idealFirstHash = vpsHash vps

    newPoolDatum     :: LiquidityPool
    emittedLiquidity :: UnpackedLiquidity
    firstHash        :: LastHash
    (newPoolDatum, emittedLiquidity, firstHash) = case txOutDatum poolOutput of
                                                    Nothing -> traceError "pool output witness missing"
                                                    Just h  -> findPoolDatum info h

    datumCorrect :: Bool 
    datumCorrect =  
      traceIfFalse "Pool record wrong"            (newPoolDatum == lp)                            && -- 8.
      traceIfFalse "Liquidity-Datum wrong"        ((unwrapUnpacked emittedLiquidity) == initBal)  && -- 9.
      traceIfFalse "initial hash wrong"           (firstHash == idealFirstHash) 

-- TODO IMPORTANT to protect against exploits: maximum slippage for swap/remove/add

{-# INLINABLE applyRevive #-}
-- moving balances from zero to positive and setting a weight
applyRevive :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
               -> Value -> Weights -> VirtualPoolState -> Value -> VirtualPoolState
applyRevive packLCs inV inW vps@VirtualPoolState{..} outV = let
    newL = calculateInitialLiquidity inV
    packed = unwrapPacked $ checkErrors $ packLCs $ Right newL
  in 
    if  traceIfFalse "reviving alive pool"                        (virtualBalances == zeroVal)           &&
        traceIfFalse "incorrect liquidity minted while reviving"  (outV == packed)                       &&
        traceIfFalse "weights don't match deposit while reviving" (inV `sameCoins` (unwrapWeights inW))  &&
        traceIfFalse "reduce weights please"                      (inW == (reduceWeights inW)) -- TODO could be simplified
    then traceError "censored"
    else traceError "invalid revival"

{-# INLINABLE applyDrain #-}
applyDrain :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
              -> Value -> VirtualPoolState -> Value -> VirtualPoolState
applyDrain packLCs inV vps@VirtualPoolState{..} outV = let 
    oldL = unwrapPacked $ checkErrors $ packLCs $ Right virtualLiquidity
    zeroL = UnpackedLiquidity zeroVal
  in
    if   traceIfFalse "incorrect liquidity redeemed while draining" (inV == oldL) &&
         traceIfFalse "incorrect balances drained"                  (outV == virtualBalances)
    then traceError "censored"
    else traceError "invalid draining"

{-# INLINABLE applySwap #-}
applySwap :: Value -> AssetClass -> VirtualPoolState -> Value -> VirtualPoolState
applySwap inV outAsset vps@VirtualPoolState{..} outV = let
    oldB = Right virtualBalances
    newB = (oldB `safeVadd` (Right inV)) `safeVsub` (Right outV)
  in
    if   traceIfFalse "swap yields incorrect asset(s)"    (outV `isSingletonOf` outAsset) &&
         traceIfFalse "value equation violated"           false -- censored
    then traceError "censored"
    else traceError "invalid swap"

{-# INLINABLE applyRemove #-}
applyRemove :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
               -> Value -> Value -> VirtualPoolState -> Value -> VirtualPoolState
applyRemove packLCs inV outIdeal ps@VirtualPoolState{..} outV = let 
    oldB = Right virtualBalances
    oldL = Right virtualLiquidity
    newB = oldB `safeVsub` (Right outV)
    newL = calculateNewLiquidity oldB virtualLiquidity newB -- checks for asset alignment
    diffL = oldL `safeLsub` newL
    packed = unwrapPacked $ checkErrors $ packLCs diffL 
  in 
    if   traceIfFalse "undesired value removed"  (outV == outIdeal)  &&
         traceIfFalse "incorrect liquidity redeemed while removing"  (inV == packed)
    then traceError "censored"
    else traceError "invalid removal"

{-# INLINABLE applyAdd #-}
applyAdd :: (Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity)
            -> Value -> VirtualPoolState -> Value -> VirtualPoolState
applyAdd packLCs inV vps@VirtualPoolState{..} outV = let
    oldB = Right virtualBalances
    oldL = Right virtualLiquidity
    newB = oldB `safeVadd` (Right inV) -- prohibits reviving (balance: 0 --> pos)
    newL = calculateNewLiquidity oldB virtualLiquidity newB -- checks for asset alignment
    diffL = newL `safeLsub` oldL -- prohibits addition of unlisted assets
    packed = unwrapPacked $ checkErrors $ packLCs diffL 
  in 
    if   traceIfFalse "incorrect liquidity minted while adding"  (outV == packed) 
    then traceError "censored"
    else traceError "invalid addition"

{-
- get pool state utxo
- get pending action utxos (remainder)
- optional: sort somehow
- iterate over those while carrying along some virtual pool state, rooted in pool state utxo, as well as output prototype, executing each pending action


TODO
- how to find all inputs to this tx?

-}
{-# INLINABLE validatePool #-}
-- validation for pool-utxo
validatePool :: Mirqur 
             -> Coin PoolState
             -> LiquidityPool
             -> UnpackedLiquidity
             -> LastHash
             -> ScriptContext
             -> Bool
validatePool mq c lp oldLiquidity lastHash ctx = 
    traceIfFalse "pool state updated incorrectly"   (actualNewState == idealNewState) &&
    traceIfFalse "chained hash updated incorrectly" (actualNewHash == idealNewHash)   && 
    traceIfFalse "not all inputs fit"               actionsFit
  where 
    info :: TxInfo
    info = scriptContextTxInfo ctx

    ownInput :: TxInInfo
    ownInput = findOwnInput' ctx

    ownAddress :: Address
    ownAddress = txOutAddress $ txInInfoResolved ownInput

    ownOutput :: TxOut
    ownOutput = case [ o
                     | o <- getContinuingOutputs ctx
                     , isUnity (txOutValue o) c
                     ] of
        [o] -> o
        _   -> traceError "expected exactly one pool output"

    findActionDatum :: TxInfo -> TxOut -> Maybe (MirqurAction, Address)
    findActionDatum info o = case txOutDatumHash o of 
        Just h            -> case findDatum h info of 
          Just (Datum d)  -> case PlutusTx.unsafeFromBuiltinData d of
            (Action m a)  -> Just (m, a)
            _             -> Nothing
          _               -> Nothing
        _                 -> Nothing
        
    -- | Get the values paid to a public key address by a pending transaction.
    outputsAt :: Address -> [Value]
    outputsAt addr = 
      let flt TxOut{txOutAddress = addr, txOutValue} = Just txOutValue
          flt _                                      = Nothing
      in mapMaybe flt (txInfoOutputs info)

    -- | Get the total value paid to a public key address by a pending transaction.
    valuePaidToAddr :: Address -> Value
    valuePaidToAddr addr = mconcat (outputsAt addr)

    -- all pending actions at this pool
    actions :: [ActionInfo]
    actions =[ ActionInfo 
              { infoInValue = txOutValue txo
              , infoActionType = action
              , infoOutValue = valuePaidToAddr addr
              , infoRank = chainedAddressHash lastHash addr -- TODO double-check if exploitable for frontrunning still
              }
            | i <- txInfoInputs info
            , let txo = txInInfoResolved i
            , txOutAddress txo == ownAddress -- better safe than sorry here for now
            , Types.isZero (txOutValue txo) c -- only want actions here, filtering pool state
            , let datum = findActionDatum info txo
            -- , datum /= Nothing -- prefer error in this case
            , let Just (action, addr) = datum
            ]

    actionsFit :: Bool
    actionsFit = all (fitsPoolAndType lp . infoActionType) actions

    packLCs :: Either BuiltinString UnpackedLiquidity -> Either BuiltinString PackedLiquidity
    packLCs = packLiquidityCoins c lp

    applyAction :: ActionInfo -> VirtualPoolState -> VirtualPoolState
    applyAction ActionInfo{..} vps = case infoActionType of 
      Revive  (LiquidityPool inW) -> applyRevive packLCs infoInValue inW      vps infoOutValue
      Drain   _                   -> applyDrain  packLCs infoInValue          vps infoOutValue
      Swap    _ outAsset          -> applySwap           infoInValue outAsset vps infoOutValue
      Remove  _ outValue          -> applyRemove packLCs infoInValue outValue vps infoOutValue
      Add     _                   -> applyAdd    packLCs infoInValue          vps infoOutValue
      _                           -> traceError  "error decoding action info"

    sortActions :: [ActionInfo] -> [ActionInfo]
    sortActions as = case as of 
      []  -> traceError  "no actions found" -- NOTE this is an important check right now
      [a] -> [a] -- TODO this should avoid computing hashes for actions, no?
      as' -> mergesort as'

    initVal, initBal :: Value
    initVal = valueWithin ownInput -- TODO brackets really redundant below or is parser lying?
    initBal = checkErrors $ (Right initVal) `safeVsub` (Right (unitValue c)) -- ensures pool token is in input


    idealNewState = foldr applyAction initState $ sortActions actions
    idealNewHash  = chainedVPSHash lastHash idealNewState


    newVal, newBal :: Value 
    newVal = txOutValue ownOutput
    newBal = checkErrors $ (Right newVal) `safeVsub` (Right $ unitValue c) -- ensures pool token is in output (doublecheck)
       

-- TODO consider refunds
{-# INLINABLE validateAction #-}
validateAction :: Coin PoolState -> MirqurAction -> Address -> ScriptContext -> Bool
--valdation for pending-action-utxo
validateAction c action addr ctx = hasPoolInput
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    hasPoolInput :: Bool
    hasPoolInput =
        traceIfFalse "Mirqur pool input expected" $
        isUnity (valueSpent info) c


{-# INLINABLE findPoolDatum #-}
findPoolDatum :: TxInfo -> DatumHash -> (LiquidityPool, UnpackedLiquidity, LastHash)
findPoolDatum info h = case findDatum h info of
    Just (Datum d) -> case PlutusTx.unsafeFromBuiltinData d of
        (Pool lp a h) -> (lp, a, h)
        _           -> traceError "error decoding pool data"
    _              -> traceError "pool input datum not found"


{-# INLINABLE mkMirqurValidator #-}
mkMirqurValidator :: Mirqur
                   -> Coin PoolState
                   -> MirqurDatum
                   -> MirqurRedeemer
                   -> ScriptContext
                   -> Bool
mkMirqurValidator mq c (Factory lps)      (CreateInFactory lp) ctx = validateCreate mq c lps lp ctx
mkMirqurValidator mq c (Pool lp lq lh)    BatchPoolActions     ctx = validatePool mq c lp lq lh ctx -- TODO lp'?
mkMirqurValidator mq c (Action act addr)  BatchAction          ctx = validateAction c act addr ctx
mkMirqurValidator _  _ _               _  _   = False

{-# INLINABLE validateLiquidityMinting #-}
validateLiquidityMinting :: Mirqur -> TokenName -> () -> ScriptContext -> Bool
validateLiquidityMinting Mirqur{..} tn _ ctx
  = case [ i
         | i <- txInfoInputs $ scriptContextTxInfo ctx
         , let v = valueWithin i
         , isUnity v mqCoin || isUnity v lpC
         ] of
    [_]    -> True
    [_, _] -> True
    _      -> traceError "pool state minting without Mirqur input"
  where
    lpC :: Coin Liquidity
    lpC = mkCoin (ownCurrencySymbol ctx) tn
